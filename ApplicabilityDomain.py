# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 14:39:31 2019

@author: US16120
"""

import pandas as pd
from pandas import Series
# from pandas.Series import astype
import numpy as np
from numpy import linalg
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.manifold import MDS
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
from scipy import spatial
from scipy.spatial import distance
from pprint import pprint
import matplotlib.pyplot as plt
import seaborn as sns
color = sns.color_palette()

df = pd.read_csv('data/processed/alles02.csv', header = 0)
df = df.drop(['Unnamed: 0'], axis=1)
df.sample(5).head()

nms = [x[0] for x in Descriptors._descList]
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
for i in range(len(df)):
    try:
        descrs = calc.CalcDescriptors(Chem.MolFromSmiles(df.iloc[i, 0]))
        for x in range(len(descrs)):
            df.at[i, str(nms[x])] = descrs[x]
    except:
        for x in range(len(descrs)):
            df.at[i, str(nms[x])] = 'NaN'   
            
df = df.replace([np.inf, -np.inf], np.nan)
df = df.dropna()
df = df.reset_index(drop=True)

train, test = train_test_split(df, test_size = 0.2, random_state = 42,
                               stratify=df[['ReadyBiodeg']])
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

X_train = train.drop(columns=['SMILES', 'EndPt', 'InChI', 'ReadyBiodeg'])
X_test = test.drop(columns=['SMILES', 'EndPt', 'InChI', 'ReadyBiodeg'])
y_train = np.ravel(train[['ReadyBiodeg']])
y_test = np.ravel(test[['ReadyBiodeg']])

def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.0)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]

corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]

scaler = StandardScaler()
scaler.fit(X_train)

X_train_std = scaler.transform(X_train)
X_test_std = scaler.transform(X_test)

#########################

col_names =  ['dist']
distances = pd.DataFrame(columns = col_names)
avgDist5 = pd.DataFrame(columns = col_names)

for i in range(len(X_train_std)):
    for j in range(len(X_train_std)):
        distances.loc[j, 'dist'] = np.linalg.norm(X_train_std[i] - X_train_std[j])
    euclid = distances.sort_values('dist').head(6).mean()
    avgDist5.loc[i, 'dist'] = euclid[0]
    if (i % 5 == 0):
        print(i)

X = train.copy()[['SMILES', 'InChI', 'EndPt', 'ReadyBiodeg']]
X['dist'] = avgDist5['dist']



avgDist5['dist'][0].describe()

from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole

pd.Series
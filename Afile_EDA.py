import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

# import csv
import pandas as pd
import numpy as np
import os
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem import AllChem
# from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import rdMolDescriptors
from rdkit.Chem.Scaffolds.MurckoScaffold import MurckoScaffoldSmiles
from rdkit.Chem.Descriptors import ExactMolWt
# from rdkit.Chem import Draw
# from rdkit.Chem.Draw import IPythonConsole
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestRegressor
# import re
from statistics import mean
import matplotlib.pyplot as plt
import pickle

current_path = os.getcwd()
print(current_path)

col_names =  ['PA_ID', 'SMILES', 'Tm']
dfA  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Tm')]
    except:
        continue

dfA.shape

dfA.sample(5).head()

def meltTemp(value):
    x = value.split(" - ")
    if len(x) == 1:
        melt_temp = float(x[0])
    else:
        y = (float(x[0]), float(x[1]))
        melt_temp = mean(y)
    return(melt_temp)

for i, row in dfA.iterrows():
    dfA.loc[i, 'avgTm'] = meltTemp(dfA.loc[i, 'Tm'])

dfA.sample(5).head()

dfA.describe()

plt.hist(dfA['avgTm'].array, bins = range(0, 500, 10))

nms = [x[0] for x in Descriptors._descList]
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
# for i in range(len(dfA)):
#     descrs = calc.CalcDescriptors(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))
#     for x in range(len(descrs)):
#         dfA.at[i, str(nms[x])] = descrs[x]
        
for i in range(len(dfA)):
    try:
        descrs = calc.CalcDescriptors(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))
        for x in range(len(descrs)):
            dfA.at[i, str(nms[x])] = descrs[x]
    except:
        for x in range(len(descrs)):
            dfA.at[i, str(nms[x])] = 'NaN'

dfA = dfA.replace([np.inf, -np.inf], np.nan)

dfA = dfA.dropna()

dfA.shape

dfA['MolWt'].describe()
plt.hist(dfA['MolWt'].array, bins = range(50, 1500, 50))
dfA['TPSA'].describe()
plt.hist(dfA['MolWt'].array, bins = range(0, 500, 10))
dfA['MolLogP'].describe()
plt.hist(dfA['MolLogP'].array, bins = range(-5, 45, 1))
dfA['NumHAcceptors'].describe()
plt.hist(dfA['NumHAcceptors'].array, bins = range(0, 30, 1))
dfA['NumHDonors'].describe()
plt.hist(dfA['NumHDonors'].array, bins = range(0, 10, 1))
dfA['FractionCSP3'].describe()
plt.hist(dfA['FractionCSP3'].array)

#  build training set & test set

train, test = train_test_split(dfA, test_size = 0.2, random_state = 42)
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

plt.hist(train['avgTm'].array, bins = range(0, 500, 10))
train[['avgTm']].describe()
plt.hist(test['avgTm'].array, bins = range(0, 500, 10))
test[['avgTm']].describe()

X_train = train.copy().drop(['PA_ID', 'SMILES', 'Tm', 'avgTm'], axis = 1)
y_train = train['avgTm'].copy()
X_test = test.copy().drop(['PA_ID', 'SMILES', 'Tm', 'avgTm'], axis = 1)
y_test = test['avgTm'].copy()

# Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.0)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]

# Identify / remove highly correlated descriptors
corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]
    
#standardize features by removing the mean and scaling to unit variance
scaler = StandardScaler()
scaler.fit(X_train)

X_train_standard = scaler.transform(X_train)
X_test_standard = scaler.transform(X_test)

from tpot import TPOTRegressor
tpot = TPOTRegressor(generations=10, population_size=50, verbosity=2)
tpot.fit(X_train_standard, y_train)
print(tpot.score(X_test_standard, y_test))
tpot.export('tpot_A_pipeline.py')

# contents of 'tpot_A_pipeline.py'

import numpy as np
import pandas as pd
from sklearn.ensemble import ExtraTreesRegressor
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline, make_union
from sklearn.svm import LinearSVR
from tpot.builtins import StackingEstimator

# NOTE: Make sure that the class is labeled 'target' in the data file
# tpot_data = pd.read_csv('PATH/TO/DATA/FILE', sep='COLUMN_SEPARATOR', dtype=np.float64)
# features = tpot_data.drop('target', axis=1).values
# training_features, testing_features, training_target, testing_target = \
#             train_test_split(features, tpot_data['target'].values, random_state=None)

# Average CV score on the training set was:-3630.154344887749
exported_pipeline = make_pipeline(
    StackingEstimator(estimator=LinearSVR(C=25.0, dual=True, epsilon=0.0001, loss="squared_epsilon_insensitive", tol=0.0001)),
    ExtraTreesRegressor(bootstrap=False, max_features=0.7000000000000001, min_samples_leaf=7, min_samples_split=9, n_estimators=100)
)

# exported_pipeline.fit(training_features, training_target)
# results = exported_pipeline.predict(testing_features)

exported_pipeline.fit(X_train_standard, y_train)
results = exported_pipeline.predict(X_test_standard)

plt.scatter(y_test, results)

# random forest regression

from sklearn.ensemble import RandomForestRegressor
forest = RandomForestRegressor(200, random_state = 42)
from pprint import pprint
pprint(forest.get_params())

from sklearn.model_selection import RandomizedSearchCV
  
# Number of trees in random forest
n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(10, 110, num = 11)]
max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# Method of selecting samples for training each tree
bootstrap = [True, False]

# Create the random grid
random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap}

pprint(random_grid)

# Use the random grid to search for best hyperparameters
# First create the base model to tune
rf = RandomForestRegressor(random_state = 42)
# Random search of parameters, using 3 fold cross validation, 
# search across 100 different combinations, and use all available cores
rf_random = RandomizedSearchCV(estimator = rf, param_distributions = random_grid,
                              n_iter = 100, scoring = 'neg_mean_absolute_error', 
                              cv = 3, verbose = 2, random_state = 42,
                              n_jobs = -1, return_train_score = True)

# Fit the random search model
rf_random.fit(X_train_standard, y_train);

rf_random.best_params_

# Create the parameter grid based on the results of random search 

from sklearn.model_selection import GridSearchCV

param_grid = {
    'bootstrap': [True],
    'max_depth': [90, 100, 110],
    'max_features': [10, 12, 14],
    'min_samples_leaf': [2, 4, 6],
    'min_samples_split': [3, 5, 7],
    'n_estimators': [800, 1000, 1200]
}

# Instantiate the grid search model
grid_search = GridSearchCV(estimator = rf, param_grid = param_grid, 
                          cv = 3, n_jobs = -1, verbose = 2, return_train_score=True)

# Fit the grid search to the data
grid_search.fit(X_train_standard, y_train);

grid_search.best_params_

param_grid = {
    'bootstrap': [True],
    'max_depth': [88, 90, 92],
    'max_features': [11, 12, 13],
    'min_samples_leaf': [2, 3],
    'min_samples_split': [6, 7, 8],
    'n_estimators': [750, 800, 850]
}

# Instantiate the grid search model
grid_search_final = GridSearchCV(estimator = rf, param_grid = param_grid, 
                                 cv = 3, n_jobs = -1, verbose = 2, return_train_score=True)

grid_search_final.fit(X_train_standard, y_train);

grid_search_final.best_params_

randomForestPreds = grid_search_final(X_test_standard)

plt.scatter(y_test, randomForestPreds)
r2_score(y_test, randomForestPreds)

import pickle
f = open('RandomForest.pkl', 'wb')
pickle.dump(grid_search_final, f)
f.close()

# simple linear regression

from sklearn.linear_model import LinearRegression
model = LinearRegression(fit_intercept = True)
model.fit(X_train_standard, y_train)
linearPreds = model.predict(X_test_standard)
plt.scatter(y_test, linearPreds)

# support vector regression

import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt

# #############################################################################
# Fit regression model
svr_rbf = SVR(kernel='rbf', C=100, gamma=0.1, epsilon=.1)
svr_lin = SVR(kernel='linear', C=100, gamma='auto')
svr_poly = SVR(kernel='poly', C=100, gamma='auto', degree=3, epsilon=.1,
               coef0=1)

svr_rbf.fit(X_train_standard, y_train)
svr_rbfPreds = svr_rbf.predict(X_test_standard)
plt.scatter(y_test, svr_rbfPreds)
r2_score(y_test, svr_rbfPreds)

svr_lin.fit(X_train_standard, y_train)
svr_linPreds = svr_lin.predict(X_test_standard)
plt.scatter(y_test, svr_linPreds)
r2_score(y_test, svr_linPreds)

svr_poly.fit(X_train_standard, y_train)
svr_polyPreds = svr_poly.predict(X_test_standard)
plt.scatter(y_test, svr_polyPreds)
r2_score(y_test, svr_polyPreds)
# #############################################################################

##########
#####     fingerprints
##########

col_names =  ['PA_ID', 'SMILES', 'Tm']
dfA  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Tm')]
    except:
        continue

def meltTemp(value):
    x = value.split(" - ")
    if len(x) == 1:
        melt_temp = float(x[0])
    else:
        y = (float(x[0]), float(x[1]))
        melt_temp = mean(y)
    return(melt_temp)

for i, row in dfA.iterrows():
    dfA.loc[i, 'avgTm'] = meltTemp(dfA.loc[i, 'Tm'])

dfA.sample(5).head()

dfA.describe()

dfA0 = dfA[dfA.avgTm > 0.0]

dfA0 = dfA0.reset_index(drop=True)

nms = [x[0] for x in Descriptors._descList]
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
for i in range(len(dfA0)):
    try:
        descrs = calc.CalcDescriptors(Chem.MolFromSmiles(dfA0.loc[i, 'SMILES']))
        for x in range(len(descrs)):
            dfA0.at[i, str(nms[x])] = descrs[x]
    except:
        for x in range(len(descrs)):
            dfA0.at[i, str(nms[x])] = 'NaN'

dfA0 = dfA0.replace([np.inf, -np.inf], np.nan)

dfA0 = dfA0.dropna()

dfA0.shape

#  build training set & test set

train, test = train_test_split(dfA0, test_size = 0.2, random_state = 42)
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

plt.hist(train['avgTm'].array, bins = range(0, 500, 10))
train[['avgTm']].describe()
plt.hist(test['avgTm'].array, bins = range(0, 500, 10))
test[['avgTm']].describe()

X_train = train.copy().drop(['PA_ID', 'SMILES', 'Tm', 'avgTm'], axis = 1)
y_train = train['avgTm'].copy()
X_test = test.copy().drop(['PA_ID', 'SMILES', 'Tm', 'avgTm'], axis = 1)
y_test = test['avgTm'].copy()

# Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.0)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]

# Identify / remove highly correlated descriptors
corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]
    
#standardize features by removing the mean and scaling to unit variance
scaler = StandardScaler()
scaler.fit(X_train)

X_train_standard = scaler.transform(X_train)
X_test_standard = scaler.transform(X_test)

from sklearn.ensemble import RandomForestRegressor
forest = RandomForestRegressor(200, random_state = 42)

forest.fit(X_train_standard, y_train)
preds = forest.predict(X_test_standard)
plt.scatter(y_test, preds)
r2_score(y_test, preds)

# Number of trees in random forest
n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(10, 110, num = 11)]
max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# Method of selecting samples for training each tree
bootstrap = [True, False]

# Create the random grid
random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap}

pprint(random_grid)

# Use the random grid to search for best hyperparameters
# First create the base model to tune
rf = RandomForestRegressor(random_state = 42)
# Random search of parameters, using 3 fold cross validation, 
# search across 100 different combinations, and use all available cores
rf_random = RandomizedSearchCV(estimator = rf, param_distributions = random_grid,
                              n_iter = 100, scoring = 'neg_mean_absolute_error', 
                              cv = 3, verbose = 2, random_state = 42,
                              n_jobs = -1, return_train_score = True)

# Fit the random search model
rf_random.fit(X_train_standard, y_train);

rf_random.best_params_

f = open('RandomForestNo0.pkl', 'wb')
pickle.dump(rf_random, f)
f.close()

f = open('RandomForestNo0.pkl', 'rb')
rf_random = pickle.load(f)
f.close()

preds = rf_random.predict(X_test_standard)
plt.scatter(y_test, preds)
r2_score(y_test, preds)

###############
#
# k Nearest Neighbors
#
###############

from sklearn import neighbors
n_neighbors = 5
knn = neighbors.KNeighborsRegressor(n_neighbors, weights = 'uniform')
knn.fit(X_train_standard, y_train)
preds = knn.predict(X_test_standard)
plt.scatter(y_test, preds)
r2_score(y_test, preds)

knn = neighbors.KNeighborsRegressor(n_neighbors, weights = 'distance')
knn.fit(X_train_standard, y_train)
preds = knn.predict(X_test_standard)
plt.scatter(y_test, preds)
r2_score(y_test, preds)

###############
#
# fingerprints
#
###############

col_names =  ['PA_ID', 'SMILES', 'Tm']
dfA  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Tm')]
    except:
        continue

def meltTemp(value):
    x = value.split(" - ")
    if len(x) == 1:
        melt_temp = float(x[0])
    else:
        y = (float(x[0]), float(x[1]))
        melt_temp = mean(y)
    return(melt_temp)

for i, row in dfA.iterrows():
    dfA.loc[i, 'avgTm'] = meltTemp(dfA.loc[i, 'Tm'])

dfA0 = dfA[dfA.avgTm > 0.0]

dfA0 = dfA0.reset_index(drop=True)

train, test = train_test_split(dfA0, test_size = 0.2, random_state = 42)
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

train_fps = []
for i in range(0, len(train)):
    m = Chem.MolFromSmiles(train.loc[i, 'SMILES'])
    # fp = MACCSkeys.GenMACCSKeys(m)
    fp = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits = 1024)
    # fp = rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect(m, nBits = 1024)
    train_fps.append(fp)
    
test_fps = []
for i in range(0, len(test)):
    m = Chem.MolFromSmiles(test.loc[i, 'SMILES'])
    # fp = MACCSkeys.GenMACCSKeys(m)
    fp = AllChem.GetMorganFingerprintAsBitVect(m, 2, nBits = 1024)
    # fp = rdMolDescriptors.GetHashedAtomPairFingerprintAsBitVect(m, nBits = 1024)
    test_fps.append(fp)
    
# helper function
def getNumpyArrays(fplist):
    nplist = []
    for fp in fplist:
        arr = np.zeros((1,), np.float32)
        DataStructs.ConvertToNumpyArray(fp, arr)
        nplist.append(arr)
    return nplist

np_train_fps = getNumpyArrays(train_fps)
np_test_fps = getNumpyArrays(test_fps)

X_train = np_train_fps
y_train = train['avgTm'].copy()
X_test = np_test_fps
y_test = test['avgTm'].copy()

rf = RandomForestRegressor(random_state = 42)
rf.fit(X_train, y_train)
preds = rf.predict(X_test)
plt.scatter(y_test, preds)
r2_score(y_test, preds)


##### scaffolds

dfA1 = dfA0.copy()

for i, row in dfA.iterrows():
    dfA.loc[i, 'scaffold'] = MurckoScaffoldSmiles(dfA.loc[i, 'SMILES'])
    
for i, row in dfA.iterrows():
    dfA.loc[i, 'MolWt'] = ExactMolWt(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))
dfA['MolWt'].describe()
plt.hist(dfA['MolWt'].array, bins = range(80, 3100, 10))
    
for i, row in dfA.iterrows():
    dfA.loc[i, 'NumHBA'] = rdMolDescriptors.CalcNumHBA(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))  
dfA['NumHBA'].describe()    
plt.hist(dfA['NumHBA'].array, bins = range(0, 30, 1))

for i, row in dfA.iterrows():
    dfA.loc[i, 'NumHBD'] = rdMolDescriptors.CalcNumHBD(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))   
dfA['NumHBD'].describe()    
plt.hist(dfA['NumHBD'].array, bins = range(0, 10, 1))
    
for i, row in dfA.iterrows():
    dfA.loc[i, 'NumRotBond'] = rdMolDescriptors.CalcNumRotatableBonds(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))
dfA['NumRotBond'].describe()    
plt.hist(dfA['NumRotBond'].array, bins = range(0, 80, 1))    
 
for i, row in dfA.iterrows():
    dfA.loc[i, 'TPSA'] = rdMolDescriptors.CalcTPSA(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))   
dfA['TPSA'].describe()    
plt.hist(dfA['TPSA'].array, bins = range(0, 480, 10))  
    
for i, row in dfA.iterrows():
    dfA.loc[i, 'CSP3'] = rdMolDescriptors.CalcFractionCSP3(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']))     
dfA['CSP3'].describe()    
plt.hist(dfA['CSP3'].array)     
    

dfA['avgTm'].describe()

print(dfA.sample(5).head())


from rdkit.Chem.EState import Fingerprinter
from rdkit.Chem.EState.Fingerprinter import FingerprintMol

FingerprintMol(Chem.MolFromSmiles(dfA.loc[0, 'SMILES']))[0]


AllChem.GetMorganFingerprintAsBitVect(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']), 2, 1024).ToBitString()
AllChem.GetMorganFingerprintAsBitVect(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']), 2, 1024)
print(AllChem.GetMorganFingerprintAsBitVect(Chem.MolFromSmiles(dfA.loc[i, 'SMILES']), 2, 1024))


qaz = AllChem.GetMorganFingerprintAsBitVect(Chem.MolFromSmiles(dfA.loc[0, 'SMILES']), 2, 1024)
lp = FingerprintMol(Chem.MolFromSmiles(dfA.loc[0, 'SMILES']))[0]
zxc = np.append(lp, qaz)
zxc

len(zxc)

len(qaz)

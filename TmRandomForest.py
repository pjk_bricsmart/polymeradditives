import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

import csv
import pandas as pd
import numpy as np
import os
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem import AllChem
from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import rdMolDescriptors
from rdkit.Chem.Scaffolds.MurckoScaffold import MurckoScaffoldSmiles
from rdkit.Chem.Descriptors import ExactMolWt
from rdkit.Chem import MACCSkeys
from rdkit.Chem.EState.Fingerprinter import FingerprintMol
from rdkit.Chem.rdMolDescriptors import GetHashedAtomPairFingerprintAsBitVect
from rdkit.Chem.rdMolDescriptors import GetHashedTopologicalTorsionFingerprintAsBitVect
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestRegressor
import re
from statistics import mean
import matplotlib.pyplot as plt
import pickle
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold

current_path = os.getcwd()
print(current_path)

df = pd.read_csv('PFile_DFT.csv')

df.describe()

nms = [x[0] for x in Descriptors._descList]
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
for i in range(len(df)):
    try:
        descrs = calc.CalcDescriptors(Chem.MolFromSmiles(df.iloc[i, 1]))
        for x in range(len(descrs)):
            df.at[i, str(nms[x])] = descrs[x]
    except:
        for x in range(len(descrs)):
            df.at[i, str(nms[x])] = 'NaN'   
            
df = df.replace([np.inf, -np.inf], np.nan)
df = df.dropna()
df = df.reset_index(drop=True)

df.head()

# remove NaN
df.dropna(inplace = True)

# train & test sets
train, test = train_test_split(df, test_size = 0.2, random_state = 350)
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

X_train = train.drop(columns=['ID', 'SMILES', 'prop', 'Formula', 'MolecularWt',
                              'Energy', 'E_HOMO', 'E_LUMO', 'Dipole', 'CPK_Area',
                              'CPK_Volume', 'PSA', 'CPK_Ovality', 'Acc_Area',
                              'LogP', 'HBD_Count', 'HBA_Count', 'ZPE', 'S',
                              'H', 'G', 'Cv'])
X_test = test.drop(columns=['ID', 'SMILES', 'prop', 'Formula', 'MolecularWt',
                              'Energy', 'E_HOMO', 'E_LUMO', 'Dipole', 'CPK_Area',
                              'CPK_Volume', 'PSA', 'CPK_Ovality', 'Acc_Area',
                              'LogP', 'HBD_Count', 'HBA_Count', 'ZPE', 'S',
                              'H', 'G', 'Cv'])
y_train = train[['LogP']]
y_test = test[['LogP']]

### Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.95):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.05)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]

### Identify / remove highly correlated descriptors
corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]

rf = RandomForestRegressor(n_estimators = 100, max_depth = 10,
                                  min_samples_split = 2,
                                  min_samples_leaf = 1,
                                  random_state = 42)

rf.fit(X_train, y_train)

pred = rf.predict(X_test)

plt.scatter(y_test, pred)

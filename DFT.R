suppressMessages(library(readxl))
suppressMessages(library(tidyverse))
suppressMessages(library(dplyr))
suppressMessages(library(magrittr))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(stringr))

dft <- readxl::read_xlsx("PA_UV_Absorber_DFT_data.xlsx", sheet = "50 molecules + thermo")

dft$Label <- str_replace(dft$Label, 'low E conf', '')
dft$Label <- str_trim(dft$Label, side = 'both')

write.csv(dft, file = 'DFT.csv')
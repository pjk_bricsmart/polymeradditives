---
title: "Polymer Additives: Spectra"
output: html_notebook
---

```{r}
library(readxl)
library(tidyverse)
library(dplyr)
library(magrittr)
library(ggplot2)
library(ggthemes)
```

```{r}
dfa <- readxl::read_xls("UVASpectrum/uv5411a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'uv5411a')
# dfa2 <- readxl::read_xls("UVASpectrum/uv5411a2.xls", skip = 6, col_names = FALSE) %>%
#   rename(wavelength = `...1`) %>%
#   rename(absorbance = `...2`) %>%
#   mutate(source = 'uv5411a2')
# dfam <- readxl::read_xls("UVASpectrum/uv5411am.xls", skip = 6, col_names = FALSE) %>%
#   rename(wavelength = `...1`) %>%
#   rename(absorbance = `...2`) %>%
#   mutate(source = 'uv5411am')
# dfb <- readxl::read_xls("UVASpectrum/uv5411b.xls", skip = 6, col_names = FALSE) %>%
#   rename(wavelength = `...1`) %>%
#   rename(absorbance = `...2`) %>%
#   mutate(source = 'uv5411b')
# dfb2 <- readxl::read_xls("UVASpectrum/uv5411b2.xls", skip = 6, col_names = FALSE) %>%
#   rename(wavelength = `...1`) %>%
#   rename(absorbance = `...2`) %>%
#   mutate(source = 'uv5411b2')
# dfbm <- readxl::read_xls("UVASpectrum/uv5411bm.xls", skip = 6, col_names = FALSE) %>%
#   rename(wavelength = `...1`) %>%
#   rename(absorbance = `...2`) %>%
#   mutate(source = 'uv5411bm')
# dfc <- readxl::read_xls("UVASpectrum/uv5411c.xls", skip = 6, col_names = FALSE) %>%
#   rename(wavelength = `...1`) %>%
#   rename(absorbance = `...2`) %>%
#   mutate(source = 'uv5411c')
# df <- rbind(dfa, dfa2, dfam, dfb, dfb2, dfbm, dfc)
```

```{r}
graf <- ggplot2::ggplot(dfa, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

```{r}
ggsave('uv5411.png', graf)
```

```{r}
dfa <- readxl::read_xls("UVASpectrum/uv9a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'uv9a')
```

```{r}
graf <- ggplot2::ggplot(dfa, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```


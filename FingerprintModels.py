import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

# import csv
import pandas as pd
import numpy as np
import os
from rdkit import Chem
# from rdkit import DataStructs
from rdkit.Chem import AllChem
# from rdkit.Chem import PandasTools
# from rdkit.Chem import Descriptors
# from rdkit.ML.Descriptors import MoleculeDescriptors
# from rdkit.Chem import rdMolDescriptors
# from rdkit.Chem.Scaffolds.MurckoScaffold import MurckoScaffoldSmiles
# from rdkit.Chem.Descriptors import ExactMolWt
from rdkit.Chem import MACCSkeys
from rdkit.Chem.EState.Fingerprinter import FingerprintMol
from rdkit.Chem.rdMolDescriptors import GetHashedAtomPairFingerprintAsBitVect
from rdkit.Chem.rdMolDescriptors import GetHashedTopologicalTorsionFingerprintAsBitVect
# from rdkit.Chem import Draw
# from rdkit.Chem.Draw import IPythonConsole
# from sklearn.preprocessing import StandardScaler
# from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestRegressor
# import re
from statistics import mean
import matplotlib.pyplot as plt
# import pickle
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold

current_path = os.getcwd()
print(current_path)

col_names =  ['PA_ID', 'SMILES', 'Tm']
dfA  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Tm')]
    except:
        continue

for i, row in dfA.iterrows():
    dfA.loc[i, 'mol'] = Chem.MolFromSmiles(dfA.loc[i, 'SMILES'])
    
def meltTemp(value):
    x = value.split(" - ")
    if len(x) == 1:
        melt_temp = float(x[0])
    else:
        y = (float(x[0]), float(x[1]))
        melt_temp = mean(y)
    return(melt_temp)

for i, row in dfA.iterrows():
    dfA.loc[i, 'avgTm'] = meltTemp(dfA.loc[i, 'Tm'])

dfA.sample(5).head()

dfA.describe()

fps = []
for i, row in dfA.iterrows():
    lp = FingerprintMol(dfA.loc[i, 'mol'])[0]
    qaz = AllChem.GetMorganFingerprintAsBitVect(dfA.loc[i, 'mol'], 2, 1024)
    zxc = np.append(lp, qaz)
    maccs = MACCSkeys.GenMACCSKeys(dfA.loc[i, 'mol'])
    zxc = np.append(zxc, maccs)
    atomprs = GetHashedAtomPairFingerprintAsBitVect(dfA.loc[i, 'mol'], 1024)
    zxc = np.append(zxc, atomprs)
    topotorsions = GetHashedTopologicalTorsionFingerprintAsBitVect(dfA.loc[i, 'mol'], 1024)
    zxc = np.append(zxc, topotorsions)
    fps.append(zxc)

X = pd.DataFrame(np.asarray(fps))
X.where(X == 0, 1, inplace = True)

X.to_csv('X1.csv')

y = dfA['avgTm']

df = X.join(y)

# remove NaN
df.dropna(inplace = True)

# train & test sets
train, test = train_test_split(df, test_size = 0.2, random_state = 350)
train = train.reset_index(drop=True)
test = test.reset_index(drop=True)

X_train = train.drop(columns=['avgTm'])
X_test = test.drop(columns=['avgTm'])
y_train = train[['avgTm']]
y_test = test[['avgTm']]

### Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X_train, 0.05)

X_train = X_train[nzv.columns]
X_test = X_test[nzv.columns]

### Identify / remove highly correlated descriptors
corr_matrix = X_train.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X_train = X_train[X_train.columns.drop(to_drop)]
X_test = X_test[X_test.columns.drop(to_drop)]

rf = RandomForestRegressor(n_estimators = 100, max_depth = 10,
                                  min_samples_split = 2,
                                  min_samples_leaf = 1,
                                  random_state = 42)

rf.fit(X_train, y_train)

pred = rf.predict(X_test)

plt.scatter(y_test, pred)

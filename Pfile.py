#!/usr/bin/env python
# coding: utf-8

# In[1]:


import platform
print(platform.python_version())


# In[2]:


import warnings
warnings.filterwarnings(action = 'once')


# In[3]:


# import dataiku
import csv
import pandas as pd
import numpy as np
# from dataiku import pandasutils as pdu
import os
from rdkit import Chem
from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
import re
from statistics import mean
import matplotlib.pyplot as plt


# In[4]:


col_names =  ['PA_ID', 'SMILES', 'prop']
dfP  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('PFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfP.loc[len(dfP)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Amax')]
    except:
        continue
    


# In[5]:


dfP.shape


# In[13]:


dfP.sample(5). head()


# In[6]:


dfP.to_csv('PFile.csv')


# In[7]:


molsP = []
with open('PFile.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        m = Chem.MolFromSmiles(row[2])
        molsP.append((m, row[1]))


# In[8]:


Draw.MolsToGridImage([m[0] for m in molsP[1:10]],
                    legends=[m[1] for m in molsP[1:10]],
                    molsPerRow=3)


# In[9]:


from rdkit import DataStructs
from rdkit.ML.Cluster import Butina


# In[10]:


fps = []
for m, pa_id in molsP[1:]:
    fps.append(Chem.RDKFingerprint(m, maxPath=5))
    
dist_matrix = []
num_fps = len(fps)
for i in range(1, num_fps):
    similarities = DataStructs.BulkTanimotoSimilarity(fps[i], fps[:i])
    dist_matrix.extend([1-x for x in similarities])
    
clusters = Butina.ClusterData(dist_matrix, num_fps, 0.5, isDistData=True)
print("number of clusters = ", len(clusters))
num_clust_g5 = len([c for c in clusters if len(c) > 5])
print("number of clusters with more than 5 compounds = ", num_clust_g5)


# In[11]:


get_ipython().run_line_magic('matplotlib', 'notebook')
import matplotlib.pyplot as plt
fig = plt.figure(1, figsize=(18,5))
plt1 = plt.subplot(111)
plt.axis([0, 151, 0, len(clusters[0])+1])
plt.xlabel('Cluster index')
plt.ylabel('Number of molecules')
plt1.bar(range(1, 151), [len(c) for c in clusters[:150]], lw = 0)
plt.show()


# In[12]:


Draw.MolsToGridImage([molsP[i][0] for i in clusters[0][:12]],
                    legends=[molsP[i][1] for i in clusters[0][:12]],
                    molsPerRow = 4)


# In[ ]:





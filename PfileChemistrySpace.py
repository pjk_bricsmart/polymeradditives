import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

import csv
import pandas as pd
import numpy as np
import os
from rdkit import Chem
from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
import re
from statistics import mean
import matplotlib.pyplot as plt
from sklearn.feature_selection import VarianceThreshold

col_names =  ['PA_ID', 'SMILES', 'prop']
dfP  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('PFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfP.loc[len(dfP)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Amax')]
    except:
        continue
    
dfP.shape

dfP.sample(5).head()

nms = [x[0] for x in Descriptors._descList]
calc = MoleculeDescriptors.MolecularDescriptorCalculator(nms)
for i in range(len(dfP)):
    try:
        descrs = calc.CalcDescriptors(Chem.MolFromSmiles(dfP.iloc[i, 1]))
        for x in range(len(descrs)):
            dfP.at[i, str(nms[x])] = descrs[x]
    except:
        for x in range(len(descrs)):
            dfP.at[i, str(nms[x])] = 'NaN'   
            
dfP = dfP.replace([np.inf, -np.inf], np.nan)
dfP = dfP.dropna()
dfP = dfP.reset_index(drop=True)

# remove NaN
dfP.dropna(inplace = True)

dfP.sample(5).head()

X = dfP.drop(columns = ['PA_ID', 'SMILES', 'prop'])
y = dfP[['prop']]

### Identify / remove near-zero variance descriptors
def variance_threshold_selector(data, threshold = 0.5):
    selector = VarianceThreshold(threshold)
    selector.fit(data)
    return data[data.columns[selector.get_support(indices = True)]]

nzv = variance_threshold_selector(X, 0.05)

X = X[nzv.columns]

corr_matrix = X.corr().abs()
upper = corr_matrix.where(np.triu(np.ones(corr_matrix.shape),
                                  k = 1).astype(np.bool))
to_drop = [column for column in upper.columns
           if any(upper[column] > 0.85)]

X = X[X.columns.drop(to_drop)]



import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

import pandas as pd
import numpy as np
import os
import statistics
import matplotlib.pyplot as plt
from rdkit import Chem
from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
from sklearn.model_selection import train_test_split

col_names = ['PA_ID', 'SMILES', 'InChI', 'InChIkey', 'Tm']
dfA = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol),
                Chem.inchi.MolToInchi(mol), Chem.inchi.MolToInchiKey(mol),
                mol.GetProp('Tm')]
    except:
        continue
    
# iterate over rows with iterrows()
for index, row in dfA.iterrows():
     # access data using column names
     if row['Tm'].find("-") == -1:
         dfA.loc[index, 'Tm'] = float(dfA.loc[index, 'Tm'])
     else:
         x = dfA.loc[index, 'Tm'].split(" - ")
         x[0] = float(x[0])
         x[1] = float(x[1])
         dfA.loc[index, 'Tm'] = statistics.mean(x)

dfA.PA_ID = dfA.PA_ID.astype(str)
dfA.SMILES = dfA.SMILES.astype(str)
dfA.InChI = dfA.InChI.astype(str)
dfA.InChIkey = dfA.InChIkey.astype(str)
dfA.Tm = dfA.Tm.astype(float)
dfA.Tm.describe()

dfA.Tm.plot(kind='hist', bins = 50)
dfA.Tm.plot(kind='density')

dfA.head()

dfA.InChIkey.describe(include = 'all')

ids = dfA["InChIkey"]
dfA_dup = dfA[ids.isin(ids[ids.duplicated()])].sort_values("InChIkey")

df = dfA[~dfA['InChIkey'].isin(dfA_dup['InChIkey'])]
df.Tm.plot(kind='density')


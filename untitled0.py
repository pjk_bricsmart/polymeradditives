# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 10:00:28 2019

@author: P J Kowalczyk
"""

%pylab inline

from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
import pandas as pd
import numpy as np

df = pd.read_csv('external_test_cases_ritu_curated.csv')

df.head()

mols = []
for index, row in df.iterrows():
    m = Chem.MolFromSmiles(row['SMILES'])
    mols.append((m, row['ID']))
  
Draw.MolsToGridImage([m[0] for m in mols],
                     legends = [m[1] for m in mols],
                     molsPerRow = 1)

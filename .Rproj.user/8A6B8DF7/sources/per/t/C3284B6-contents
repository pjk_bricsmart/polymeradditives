suppressMessages(library(readxl))
suppressMessages(library(tidyverse))
suppressMessages(library(dplyr))
suppressMessages(library(magrittr))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
suppressMessages(library(MESS))
suppressMessages(library(stringr))
suppressMessages(library(officer))

#####

SPECTRA <- read_pptx() 

results <- data.frame(
  CMPD = character(),
  MaxAbsorbance = character(),
  MaxAbsWavelength = numeric(),
  AreaUnderCurve = numeric(),
  stringsAsFactors = FALSE
) 

spectra <- list.files('UVASpectrum')

j <- 0

for (i in seq(1:length(spectra)))
# for (i in seq(1:20))
{
  fileName <- str_split(spectra[i], '\\.')
  if (fileName[[1]][2] != "pdf") {
    
    fullPath <- paste0('UVASpectrum/', spectra[i])
    print(fullPath)
    cmpd <- fileName[[1]][1]
    
    df <-
      # readxl::read_xls("UVASpectrum/uv9c.xls",
      readxl::read_xls(fullPath,
                       skip = 6,
                       col_names = FALSE) %>%
      rename(wavelength = `...1`) %>%
      rename(absorbance = `...2`) %>%
      select(wavelength, absorbance) %>%
      mutate(source = cmpd) %>%
      na.omit() %>%
      data.frame()
    
    MINIMUM <-
      ifelse(min(df$wavelength) > 290, min(df$wavelength), 290)
    MAXIMUM <-
      ifelse(max(df$wavelength) < 400, max(df$wavelength), 400)
    
    df2 <- df[df$wavelength > MINIMUM, ]
    df2 <- df2[df2$wavelength < MAXIMUM, ]
    
    maxAbs <- df2 %>% filter(absorbance == max(df2$absorbance))
    maxAbs <- maxAbs[1, ]
    
    areaUnderCurve <-
      MESS::auc(
        df$wavelength,
        df$absorbance,
        from = MINIMUM,
        to = MAXIMUM,
        type = 'linear'
      )
    
    MAXABS <-
      paste0('Maximum absorbance = ', maxAbs$absorbance, '\n')
    MAXWAVE <-
      paste0('Wavelength at maximum absorbance = ',
             maxAbs$wavelength,
             '\n')
    AREA <- paste0('Area under curve = ', areaUnderCurve)
    alles <- paste0(MAXABS, MAXWAVE, AREA)
    
    j <- j + 1
    results[j, 'CMPD'] <- cmpd
    results[j, 'MaxAbsorbance'] <- maxAbs$absorbance
    results[j, 'MaxAbsWavelength'] <- maxAbs$wavelength
    results[j, 'AreaUnderCurve'] <- areaUnderCurve

    graf2 <-
      ggplot2::ggplot(df, aes(x = wavelength, y = absorbance)) +
      geom_area(aes(x = wavelength, y = absorbance),
                data = df2,
                fill = "#ededed") +
      geom_line(color = "#26294d") +
      geom_vline(xintercept = maxAbs$wavelength, color = "#f59b3f") +
      geom_segment(aes(
        x = 290,
        y = maxAbs$absorbance,
        xend = 400,
        yend = maxAbs$absorbance
      ),
      color = "#f59b3f") +
      labs(title = cmpd,
           subtitle = alles) +
      ggthemes::theme_tufte()
    # plot(graf2)
    
    SPECTRA <- add_slide(SPECTRA)
    SPECTRA <- ph_with(x = SPECTRA, value = graf2, 
                       location = ph_location_fullsize() )
    
  }
}

print(SPECTRA, target = "SPECTRA02.pptx")
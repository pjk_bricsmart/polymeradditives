---
title: "Polymer Additives"
subtitle: "Spectra"
author: "Paul J. Kowalczyk"
date: "`r Sys.Date()`"
output: slidy_presentation
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r, warning = FALSE, echo = FALSE}
suppressMessages(library(readxl))
suppressMessages(library(tidyverse))
suppressMessages(library(dplyr))
suppressMessages(library(magrittr))
suppressMessages(library(ggplot2))
suppressMessages(library(ggthemes))
```

```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/uv5411a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'uv5411a')
```

## CYASORB UV-5411
```{r, echo = FALSE, warning = FALSE}
graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/Chi326a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'Chi326a')
```

## Chi 326
```{r, echo = FALSE, warning = FALSE}
graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/Chi328a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'Chi328a')
```

## Chi 328
```{r, echo = FALSE, warning = FALSE}
graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p1164a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p1164a')
```

## p1164
```{r, echo = FALSE, warning = FALSE}
graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p2098
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p2098a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p2098a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p2340
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p2340a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p2340a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p2417
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p2417a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p2417a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p2630
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p2630a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p2630a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3638
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3638a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3638a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3842
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3842a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3842a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3843
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3843a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3843a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3855
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3855a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3855a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3856
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3856a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3856a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3878
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3878a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3878a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3883
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3883a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3883a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3893
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3893a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3893a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3950
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3950a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3950a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3981
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3981a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3981a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3988
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3988a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3988a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3990
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3990a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3990a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p3991
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3991a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3991a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufteomnis()
graf
```

## p3996
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p3996a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p3996a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p4226
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p4226a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p4226a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```

## p4231
```{r, echo = FALSE, warning = FALSE, message = FALSE}
df <- readxl::read_xls("UVASpectrum/p4231a.xls", skip = 6, col_names = FALSE) %>%
  rename(wavelength = `...1`) %>%
  rename(absorbance = `...2`) %>%
  mutate(source = 'p4231a')

graf <- ggplot2::ggplot(df, aes(x = wavelength, y = absorbance, color = source)) + geom_line() + ggthemes::theme_tufte()
graf
```
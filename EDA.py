import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

import csv
import pandas as pd
import numpy as np
import os
from rdkit import Chem
from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole
import re
from statistics import mean
import matplotlib.pyplot as plt

col_names =  ['PA_ID', 'SMILES', 'Tm']
dfA  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Tm')]
    except:
        continue

dfA.shape

dfA.sample(5).head()

# txt = dfA.loc[150, 'Tm']
# x = txt.split(" - ")
# if len(x) == 1:
#     melt_temp = float(x[0])
# else:
#     y = (float(x[0]), float(x[1]))
#     melt_temp = mean(y)

def meltTemp(value):
    x = value.split(" - ")
    if len(x) == 1:
        melt_temp = float(x[0])
    else:
        y = (float(x[0]), float(x[1]))
        melt_temp = mean(y)
    return(melt_temp)

for i, row in dfA.iterrows():
    dfA.loc[i, 'avgTm'] = meltTemp(dfA.loc[i, 'Tm'])

dfA.sample(5).head()

dfA.describe()

plt.hist(dfA['avgTm'].array, bins = range(0, 500, 10))

dfA.to_csv('AFile.csv')

dfA.loc[[0]]

molsA = []
with open('AFile.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        m = Chem.MolFromSmiles(row[2])
        molsA.append((m, row[1]))

row

Draw.MolsToGridImage([m[0] for m in molsA[1:10]],
                    legends=[m[1] for m in molsA[1:10]],
                    molsPerRow=3)

col_names =  ['PA_ID', 'SMILES', 'prop']
dfP  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('PFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfP.loc[len(dfP)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Amax')]
    except:
        continue
    


# In[20]:


dfP.shape


# In[21]:


dfP.to_csv('PFile.csv')


# In[22]:


molsP = []
with open('PFile.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        m = Chem.MolFromSmiles(row[2])
        molsP.append((m, row[1]))


# In[23]:


Draw.MolsToGridImage([m[0] for m in molsP[1:10]],
                    legends=[m[1] for m in molsP[1:10]],
                    molsPerRow=3)


# In[24]:


molsP[1:10]


# In[25]:


dfP.shape


# In[26]:


from rdkit import DataStructs
from rdkit.ML.Cluster import Butina


# In[27]:


fps = []
for m, pa_id in molsP[1:]:
    fps.append(Chem.RDKFingerprint(m, maxPath=5))
    
dist_matrix = []
num_fps = len(fps)
for i in range(1, num_fps):
    similarities = DataStructs.BulkTanimotoSimilarity(fps[i], fps[:i])
    dist_matrix.extend([1-x for x in similarities])
    
clusters = Butina.ClusterData(dist_matrix, num_fps, 0.5, isDistData=True)
print("number of clusters = ", len(clusters))
num_clust_g5 = len([c for c in clusters if len(c) > 5])
print("number of clusters with more than 5 compounds = ", num_clust_g5)


# In[28]:


get_ipython().run_line_magic('matplotlib', 'notebook')
import matplotlib.pyplot as plt
fig = plt.figure(1, figsize=(18,5))
plt1 = plt.subplot(111)
plt.axis([0, 151, 0, len(clusters[0])+1])
plt.xlabel('Cluster index')
plt.ylabel('Number of molecules')
plt1.bar(range(1, 151), [len(c) for c in clusters[:150]], lw = 0)
plt.show()


# In[29]:


Draw.MolsToGridImage([molsP[i][0] for i in clusters[0][:12]],
                    legends=[molsP[i][1] for i in clusters[0][:12]],
                    molsPerRow = 4)


# In[ ]:





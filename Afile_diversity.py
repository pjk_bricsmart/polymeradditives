# -*- coding: utf-8 -*-
"""
Created on Thu Oct  3 11:16:01 2019

@author: US16120 [PJKowalczyk]
"""

import platform
print(platform.python_version())

import warnings
warnings.filterwarnings(action = 'once')

# import csv
import pandas as pd
import numpy as np
import os
from rdkit import Chem
from rdkit import DataStructs
from rdkit.Chem import AllChem
# from rdkit.Chem import PandasTools
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors
from rdkit.Chem import rdMolDescriptors
from rdkit.Chem.Scaffolds.MurckoScaffold import MurckoScaffoldSmiles
from rdkit.Chem.Descriptors import ExactMolWt
# from rdkit.Chem import Draw
# from rdkit.Chem.Draw import IPythonConsole
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestRegressor
# import re
from statistics import mean
import matplotlib.pyplot as plt
import pickle

current_path = os.getcwd()
print(current_path)

col_names =  ['PA_ID', 'SMILES', 'Tm']
dfA  = pd.DataFrame(columns = col_names)

suppl = Chem.SDMolSupplier('AFile.sdf')
for mol in suppl:
    if mol is None: continue
    if mol.GetNumAtoms() < 1: continue
    try:
        dfA.loc[len(dfA)] = [mol.GetProp('PA_ID'), Chem.MolToSmiles(mol), mol.GetProp('Tm')]
    except:
        continue

dfA.shape

dfA.sample(5).head()
